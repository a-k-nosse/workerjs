(function () {
  let listContainer = document.getElementById('list-container');
  let uList = document.getElementById('uList');

  let clone = document.getElementById('clone');
  let normal = document.getElementById('normal');

  let iterations = 1000;

  let fnChangeList = (nodeList) => {
    for (let i = 0; i < iterations; i++) {
      let itemList = nodeList.childNodes;
      let itemLength = itemList.length;
      for (let j = 0; j < itemLength; j++) {
        itemList[j].innerHTML += ' ->  ' + (i + j);
      }
    }
  };

  clone.addEventListener('click', ()=> {

    let cloneUl = uList.cloneNode(true);
    listContainer.removeChild(uList);
    let t0 = performance.now();
    fnChangeList(cloneUl);
    let t1 = performance.now();
    listContainer.appendChild(cloneUl);
    console.log('Clone time: ', (t1 - t0));
  });

  normal.addEventListener('click', ()=> {
    let t0 = performance.now();
    fnChangeList(uList);
    let t1 = performance.now();

    console.log('Normal time: ', (t1 - t0));
  });
})();
