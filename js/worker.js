importScripts('sjcl.js');
addEventListener('message', (e)=> {

  var key = sjcl.misc.pbkdf2(e.data.password, e.data.salt, e.data.iteraction, 256);
  postMessage(key);
});